# DNSSEC Helpers

These are the scripts I use to keep my DNSSEC deployment running.
I use `nsd` as my authoritive DNS server.
In contrast to the `bind` server, `nsd` expects plain zone files which can optionally be signed by an external tool.
These scripts can be used to sign a plain zone file which can be served by `nsd`.

## Usage

I use this command to resign all my zones:

```
# dnssec.sh -s -b -v -k /etc/nsd/dnskeys
```

For further information use `-h`.
