#!/bin/bash

set -u

ALGORITHM="${DNSSEC_ALGORITHM:-ECDSAP256SHA256}"
KEYDIR="${DNSSEC_KEYDIR:-/etc/dnskeys}"
ZONE="example.org"
ZONEDIR="/etc/nsd/zones"
BATCH="n"
VERBOSE="n"
DRYRUN="n"
INCSERIAL="n"

log() {
	if [[ "$VERBOSE" == 'y' ]]; then
		echo "$@"
	fi
}

# $1: keyarray (output)
# $2: keydir
# $3: zone
findkeys() {
	local -n out=$1
	mapfile raw_keys -t < <(find "$2" -name "K$3.*.private")
	if [[ ${#raw_keys[@]} == 0 ]]; then
		log "no keys for zone '$3' found"
		return 1
	fi
	for key in "${raw_keys[@]}"; do
		out+=(${key%.*})
	done

	log "found keys: ${out[*]}"
	return 0
}

# $1    : zonefile
# $2..$n: keyfiles
signzone() {
	local z="$1"
	shift

	log "signing zone '$z' with keys: $*"
	ldns-signzone -n -p "$z" -f "${z}.signed" "$@"
}

# $1: file
get_newserial() {
	awk -v today="$(date +%Y%m%d)" 'match($0, /([0-9]{8})([0-9]+)\s+;\s+serial/, m) {
		date = m[1]
		counter = m[2]
		if (date == today) {
			counter++
		} else {
			counter = 1
		}
		printf("%s%03d  ; serial", today, counter)
	}' "$1"
}

# $1: file
incserial() {
	local f="$1"
	local serial
	serial="$(get_newserial "$f")"

	if [[ "$DRYRUN" == "n" ]]; then
		sed -i "/; serial/c$serial" "$f"
	fi
}

cmd_signzone() {
	if [[ "$BATCH" == 'y' ]]; then
		for zonefile in "$ZONEDIR"/*.zone; do
			local keys
			local zone
			keys=()
			zone="$(basename "${zonefile%.*}")"

			if ! findkeys keys "$KEYDIR/$zone" "$zone"; then
				log "skipping…"
				continue
			fi

			if [[ "$DRYRUN" == "n" ]]; then
				if [[ "$INCSERIAL" == "y" ]]; then
					incserial "$zonefile"
				fi

				signzone "$zonefile" "${keys[@]}"
			fi
		done
	else
		local keys
		keys=()
		if ! findkeys keys "$KEYDIR/$ZONE" "$ZONE"; then
			log "failed"
			return 1
		fi

		if [[ "$DRYRUN" == "n" ]]; then
			if [[ "$INCSERIAL" == "y" ]]; then
				incserial "$ZONEDIR/$ZONE.zone"
			fi

			signzone "$ZONEDIR/$ZONE.zone" "${keys[@]}"
		fi
	fi
}

cmd_genkeys() {
	if [[ ! -d "$KEYDIR/$ZONE" ]]; then
		mkdir -p "$KEYDIR/$ZONE"
	fi

	cd "$KEYDIR/$ZONE" || return 1

	umask 077
	ldns-keygen -a "$ALGORITHM" -n ZONE "$ZONE"
	ldns-keygen -f KSK -a "$ALGORITHM" -n ZONE "$ZONE"
}

usage() {
	echo "usage: $(basename "$0") [OPERATION] [OPTIONS]"
	echo ""
	echo "operations:"
	echo " -g   Generate new DNSSEC keys"
	echo " -s   Sign a particular zone"
	echo ""
	echo "options:"
	echo " -a   Specify the DNSSEC algorithm (default: $ALGORITHM)"
	echo " -b   Batch process all zones in ZONEDIR (see -d)"
	echo " -k   Specify the keydir (default: $KEYDIR)"
	echo " -d   Specify the zonedir (default: $ZONEDIR)"
	echo " -r   Dryrun; do not sign anything (combine with -v)"
	echo " -z   Specify the zone (default: $ZONE)"
}

operation=""
while getopts "gsa:bd:k:rz:vhi" arg; do
	case "$arg" in
		# operations
		g) operation="gen";;
		s) operation="sig";;
		# r) cmd_rollover;;
		# c) cmd_check;;

		# options
		a)  ALGORITHM="$OPTARG";;
		b)  BATCH="y";;
		d)  ZONEDIR="$OPTARG";;
		k)  KEYDIR="$OPTARG";;
		i)  INCSERIAL="y";;
		z)  ZONE="$OPTARG";;
		r)  DRYRUN="y";;
		v)  VERBOSE="y";;

		# help stuff
		h)  usage && exit 0;;
		*)  usage && exit 1;;
	esac
done

case "$operation" in
	gen) cmd_genkeys;;
	sig) cmd_signzone;;
	*)   usage && exit 1;;
esac
